
extern "C"
{
#include "FreeRTOS.h"
#include "timers.h"
#include <cortexm/exception-handlers.h>
}

#include "SPI.hpp"
#include "W5500.h"

#define WRITE_BIT_POS 0x04

static void DataRxTransferComplete();
static void DataTxTransferComplete();

WIZnet_Chip* WIZnet_Chip::inst;
SPI SPI4_Interface;
SPI_HandleTypeDef hpspi4;

extern DMA_HandleTypeDef hdma_spi4_rx;
extern DMA_HandleTypeDef hdma_spi4_tx;

WIZnet_Chip::WIZnet_Chip()
{
	m_SPIIterface = nullptr;
	hpspi4.Instance = SPI4;

	mac[0] = 0x00;
	mac[1] = 0x12;
	mac[2] = 0x34;
	mac[3] = 0x56;
	mac[4] = 0x78;
	mac[5] = 0xFF;

	ip[0] = 192;
	ip[1] = 168;
	ip[2] = 1;
	ip[3] = 10;

	netmask[0] = 255;
	netmask[1] = 255;
	netmask[2] = 255;
	netmask[3] = 0;

	gateway[0] = 192;
	gateway[1] = 168;
	gateway[2] = 1;
	gateway[3] = 1;

    dnsaddr[0] = 0x00;
    dnsaddr[1] = 0x00;
    dnsaddr[2] = 0x00;
    dnsaddr[3] = 0x00;

    dhcp = false;

    m_SocketsInUse = 0x00;
    m_SocketIRqStatus[0] = 0x00; //ServerStatus
    m_SocketIRqStatus[1] = 0x00; //ClientStatus
    m_SocketIrqCallback[0] = nullptr;
    m_SocketIrqCallback[1] = nullptr;
}

WIZnet_Chip::~WIZnet_Chip()
{
    delete m_SPIIterface;
}

bool WIZnet_Chip::Init()
{
	WIZnet_Chip::inst = this;

	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();

	GPIO_InitTypeDef GPIO_InitStruct;

	//Init IrQ pin ->  PF2
	GPIOF->BSRR |= GPIO_BSRR_BS2;
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(EXTI2_IRQn, 6, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);

	//Init Reset pin ->  PE3
	GPIOC->BSRR |= GPIO_BSRR_BS3;
	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	//Init CS pin ->  PE4
	GPIOA->BSRR |= GPIO_BSRR_BS4;
	GPIO_InitStruct.Pin = GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	SPI4_Interface.Init(&hpspi4);
	m_SPIIterface = &SPI4_Interface;

	uint8_t prescVal = m_SPIIterface->CalcPrescaler(1000000);
	m_SPIIterface->SetPrescaler(prescVal);

	ResetChip();

	//MAC Address
	WriteRegister(BSB_COMMON_REG, SHAR, mac, 6U);

	//Gateway
	WriteRegister(BSB_COMMON_REG, GAR, gateway, 4U);

	//Mask
	WriteRegister(BSB_COMMON_REG, SUBR, netmask, 4U);

	//IP
	WriteRegister(BSB_COMMON_REG, SIPR, ip, 4U);

	return true;
}

void WIZnet_Chip::ResetChip()
{
	GPIOE->BSRR |= GPIO_BSRR_BR3;
	vTaskDelay(1);
	GPIOE->BSRR |= GPIO_BSRR_BS3;
	vTaskDelay(1);
}

void WIZnet_Chip::EnableChip()
{
	GPIOE->BSRR |= GPIO_BSRR_BR4;
}

void WIZnet_Chip::DisableChip()
{
	GPIOE->BSRR |= GPIO_BSRR_BS4;
}

uint32_t WIZnet_Chip::ReadRegister(uint8_t BSB, uint16_t regAddress, uint8_t* data, uint8_t length)
{
	uint32_t retVala = 0x00;

	uint8_t rxBuff[length + 3U];
	uint8_t txBuff[3U];
	CreateFrameHeader(txBuff, BSB, regAddress, BUFFER_READ);

	EnableChip();
	retVala = m_SPIIterface->WriteRead(rxBuff, txBuff, (length + 3U));
	DisableChip();

	memcpy(data, &rxBuff[3], length);

	return retVala;
}

uint32_t WIZnet_Chip::WriteRegister(uint8_t BSB, uint16_t regAddress, uint8_t* data, uint8_t length)
{
	uint32_t retVala = 0x00;
	if(length > 0U)
	{
		uint8_t txBuff[length + 3U];
		CreateFrameHeader(txBuff, BSB, regAddress, BUFFER_WRITE);
		memcpy(&txBuff[3], data, length);

		EnableChip();
		retVala = m_SPIIterface->Write(txBuff, (length + 3U));
		DisableChip();
	}

	return retVala;
}

void WIZnet_Chip::CreateFrameHeader(uint8_t* const header, uint8_t const BSB, uint16_t const regAddress, TransferDirection const direction)
{
	header[0] = static_cast<uint8_t>(regAddress >> 8U);
	header[1] = static_cast<uint8_t>(regAddress & 0x00FF);

	if(direction == BUFFER_WRITE)
	{
		header[2] = static_cast<uint8_t>(((BSB << 3) & 0xF8) | WRITE_BIT_POS);
	}
	else
	{
		header[2] = static_cast<uint8_t>(((BSB << 3) & 0xF8));
	}
}

bool WIZnet_Chip::SetProtocol(uint8_t socket, Protocol p)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	WriteRegister(regAddress, Sn_MR, reinterpret_cast<uint8_t*>(&p), 1U);
    return true;
}

bool WIZnet_Chip::SocketCommand(uint8_t socket, Command p)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	WriteRegister(regAddress, Sn_CR, (uint8_t*)&p, 1U);
    return true;
}

bool WIZnet_Chip::GetSocketStatus(uint8_t socket, uint8_t* value)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	ReadRegister(regAddress, Sn_SR, value, 1U);
	return true;
}

bool WIZnet_Chip::SetSourcePort(uint8_t socket, uint16_t port)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	uint8_t data[2];
	data[0] = port >> 8U;
	data[1] = port & 0x00FF;
	WriteRegister(regAddress, Sn_PORT, data, 2U);
    return true;
}

bool WIZnet_Chip::SetDestPort(uint8_t socket, uint16_t port)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	uint8_t data[2];
	data[0] = port >> 8U;
	data[1] = port & 0x00FF;
	WriteRegister(regAddress, Sn_DPORT, data, 2U);
    return true;
}

bool WIZnet_Chip::GetSocketInteruptReg(uint8_t socket, uint8_t* data)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	ReadRegister(regAddress, Sn_IR, data, 1U);
	return true;
}

bool WIZnet_Chip::ResetSocketInteruptStatus(uint8_t socket, uint8_t* value)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	WriteRegister(regAddress, Sn_IR, value, 1U);
	return true;
}

bool WIZnet_Chip::GetRecievedMessageSize(uint8_t socket, uint16_t* size)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	uint8_t data[2];
	ReadRegister(regAddress, Sn_RX_RSR, data, 2U);
	*size = (data[1] | (data[0]<< 8));
	return true;
}

bool WIZnet_Chip::GetBufferFreeSpace(uint8_t socket, uint16_t* space)
{
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	uint8_t data[2];
	ReadRegister(regAddress, Sn_TX_FSR, data, 2U);
	*space = (data[1] | (data[0]<< 8));
	return true;
}

bool WIZnet_Chip::SetBufferSize(uint8_t socket, BufferType type, BufferSize size)
{
	uint8_t opVal = 0xFF;
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	uint8_t data = (uint8_t)size;

	switch(type)
	{
		case RX_BUFFER: opVal = WriteRegister(regAddress, Sn_RXBUF_SIZE, &data, 1U); break;
		case TX_BUFFER: opVal = WriteRegister(regAddress, Sn_TXBUF_SIZE, &data, 1U); break;
		default: break;
	}

	return ((0U == opVal) ? true : false);
}

bool WIZnet_Chip::GetBufferSize(uint8_t socket, BufferType type, BufferSize* size)
{
	uint8_t opVal = 0xFF, regVal;
	bool retVal = false;
	const uint8_t regAddress = GetSocketRegisterAddress(socket);

	switch(type)
	{
		case RX_BUFFER: opVal = ReadRegister(regAddress, Sn_RXBUF_SIZE, &regVal, 1U); break;
		case TX_BUFFER: opVal = ReadRegister(regAddress, Sn_TXBUF_SIZE, &regVal, 1U); break;
		default: break;
	}

	if(opVal == 0)
	{
		(*size) =  (BufferSize)regVal;
		retVal = true;
	}

	return retVal;
}

bool WIZnet_Chip::InitSocket(uint8_t socket, Protocol interface, BufferSize RxBufSize, BufferSize TxBufSize, uint16_t sourcePort, uint16_t destinationPort)
{
	bool retVal = false;
	if(SocketInUse(socket) == false )
	{
		m_SocketsInUse |= (1<<socket);

		SetProtocol(socket, interface);
		SetSourcePort(socket, sourcePort);
		SetDestPort(socket, destinationPort);
		SetBufferSize(socket, RX_BUFFER, RxBufSize);
		SetBufferSize(socket, TX_BUFFER, TxBufSize);

		// Activate socket interupts
		uint8_t rVal = 0x00;
		const uint8_t regAddress = GetSocketRegisterAddress(socket);
		rVal = INT_CON | INT_DISCON | INT_RECV | INT_SEND_OK;
		WriteRegister(regAddress, Sn_IR, &rVal, 1U);

		// Update sockets interupts mask
		rVal = m_SocketsInUse;
		WriteRegister(BSB_COMMON_REG, SIMR, &rVal, 1U);

		// Open socket
		SocketCommand(socket, OPEN);
		retVal = true;
	}
	return retVal;
}

bool WIZnet_Chip::SocketInUse(uint8_t socket)
{
	return (((m_SocketsInUse & (1<< socket)) != 0) ? true : false );
}

bool WIZnet_Chip::SubscribeSocketIrqHandler(uint8_t socket, t_WizzNetIrqCallback cbFunction)
{
	bool retVal = false;
	if(socket < SOCKET_IN_USE)
	{
		m_SocketIrqCallback[socket] = cbFunction;
		retVal = true;
	}
	return retVal;
}

bool WIZnet_Chip::GetBufferDataPointer(uint8_t socket, BufferType type, uint16_t* pData)
{
	uint8_t opVal = 0xFF, regVal[2];
	bool retVal = false;
	const uint8_t regAddress = GetSocketRegisterAddress(socket);
	switch(type)
	{
		case RX_BUFFER: opVal = ReadRegister(regAddress, Sn_RX_RD, regVal, 2U); break;
		case TX_BUFFER: opVal = ReadRegister(regAddress, Sn_TX_WR, regVal, 2U); break;
		default: break;
	}

	if(opVal == 0)
	{
		*pData = (regVal[1] | (regVal[0]<< 8));
		retVal = true;
	}
	return retVal;
}

bool WIZnet_Chip::SetBufferDataPointer(uint8_t socket, BufferType type, uint16_t pData)
{
	uint8_t opVal = 0xFF, regVal[2];
	bool retVal = false;
	const uint8_t regAddress = GetSocketRegisterAddress(socket);

	regVal[0] = (pData >> 8);
	regVal[1] = (pData & 0xFF);

	switch(type)
	{
		case RX_BUFFER: opVal = WriteRegister(regAddress, Sn_RX_RD, regVal, 2U); break;
		case TX_BUFFER: opVal = WriteRegister(regAddress, Sn_TX_WR, regVal, 2U); break;
		default: break;
	}

	if(opVal == 0)
	{
		retVal = true;
	}
	return retVal;
}

void WIZnet_Chip::InterruptHandler()
{
	uint8_t regVal, socketMask, socketIndex;
	ReadRegister(BSB_COMMON_REG, SIR, &regVal, 1U);
	regVal = regVal & m_SocketsInUse;

	for(socketIndex = 0U; (socketIndex < 7U) && (0U != regVal); socketIndex++)
	{
		socketMask = (1<<socketIndex);
		if((regVal & socketMask) != 0U)
		{
			GetSocketInteruptReg(socketIndex, &m_SocketIRqStatus[socketIndex]);
			ResetSocketInteruptStatus(socketIndex, &m_SocketIRqStatus[socketIndex]);

			if(nullptr != m_SocketIrqCallback[socketIndex])
			{
				m_SocketIrqCallback[socketIndex](m_SocketIRqStatus[socketIndex]);
			}

			regVal = regVal & (~socketMask);
		}
	}
}

bool WIZnet_Chip::WriteTxBuffer(uint8_t socket, uint16_t dataPointer, uint8_t* txData, uint16_t length, t_WizzNetCallback transferCompletCb)
{
	bool retVal = false;
	retVal = SetupDataTransfer(socket, dataPointer, txData, length, BUFFER_WRITE, transferCompletCb);
	return retVal;
}

bool WIZnet_Chip::ReadRxBuffer(uint8_t socket, uint16_t dataPointer, uint8_t* txData, uint16_t length, t_WizzNetCallback transferCompletCb)
{
	bool retVal = false;
	retVal = SetupDataTransfer(socket, dataPointer, txData, length, BUFFER_READ, transferCompletCb);
	return retVal;
}

bool WIZnet_Chip::SetupDataTransfer(uint8_t socket, uint16_t dataPointer, uint8_t* txData, uint16_t length, const TransferDirection direction, t_WizzNetCallback transferCompletCb)
{
	bool retVal = false;
	if(m_TransferCtrlBusy == false)
	{
		m_TransferCtrlBusy = true;

		uint8_t BSB = 0U;
		if(direction == TransferDirection::BUFFER_READ)
		{
			BSB = (BSB_SOC0_RxBUFF + (socket * 0x04));
			CreateFrameHeader(m_TransferDataHeader, BSB, dataPointer, BUFFER_READ);
		}
		else if ((direction == TransferDirection::BUFFER_WRITE))
		{
			BSB = (BSB_SOC0_TxBUFF + (socket * 0x04) );
			CreateFrameHeader(m_TransferDataHeader, BSB, dataPointer, BUFFER_WRITE);
		}

		m_DataTransferLenght = length;
		m_pDataTransferBuffer = txData;
		m_TransferDirection = direction;

		if(transferCompletCb != nullptr)
			m_pDataTransferCplCallback = transferCompletCb;

		DataTransferController(TransferEvent::HEADER_PREPARED);

		retVal = true;
	}

	return retVal;
}

uint8_t WIZnet_Chip::GetSocketRegisterAddress(uint8_t socket)
{
	return (BSB_SOC0_REG + (socket * BSB_SOCKET_ADDRESS_OFFSET));
}

void WIZnet_Chip::DataTransferController(TransferEvent event)
{
	switch(m_TransferState)
	{
		case HEADER_TRANSFER:
		{
			if (HEADER_PREPARED == event)
			{
				EnableChip();
				m_SPIIterface->WriteDMA(m_TransferDataHeader, 3U);
				m_TransferState = DATA_TRANSFER;
			}
			break;
		}

		case DATA_TRANSFER:
		{
			if(event == TRANSMIT_COMPLETE)
			{
				if(m_TransferDirection == BUFFER_WRITE)
				{
					m_SPIIterface->WriteDMA(m_pDataTransferBuffer,
							m_DataTransferLenght);
				}
				else if(m_TransferDirection == BUFFER_READ)
				{
					m_SPIIterface->ReadDMA(m_pDataTransferBuffer,
							m_DataTransferLenght);
				}
				m_TransferState = CLOSE_TRANSFER;
			}
			break;
		}

		case CLOSE_TRANSFER:
		{
			if((event == TRANSMIT_COMPLETE) || (event == RECIEVE_COMPLETE))
			{
				DisableChip();
				//calback finish transfer
				if(m_pDataTransferCplCallback != nullptr)
					m_pDataTransferCplCallback();

				m_TransferState = HEADER_TRANSFER;
				m_TransferCtrlBusy = false;
			}
			break;
		}
		default: break;
	}
}

static void DataRxTransferComplete()
{
	WIZnet_Chip* WizzNetChip;
	WizzNetChip = WIZnet_Chip::getInstance();
	WizzNetChip->DataTransferController(TransferEvent::RECIEVE_COMPLETE);
}

static void DataTxTransferComplete()
{
	WIZnet_Chip* WizzNetChip;
	WizzNetChip = WIZnet_Chip::getInstance();
	WizzNetChip->DataTransferController(TransferEvent::TRANSMIT_COMPLETE);
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi == &hpspi4)
	{
		DataTxTransferComplete();
	}
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi == &hpspi4)
	{
		DataRxTransferComplete();
	}
}

//Rx SPI Transfer
extern "C" {void DMA2_Stream0_IRQHandler(void);};
void DMA2_Stream0_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&hdma_spi4_rx);
}

//Tx SPI Transfer
extern "C" {void DMA2_Stream1_IRQHandler(void);};
void DMA2_Stream1_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&hdma_spi4_tx);
}

// External Line[15:10]
// PF12 Line interrupt
extern "C" {void EXTI2_IRQHandler(void);};
extern void EXTI2_IRQHandler(void)
{
	// clear pending interrupt bits
	uint32_t regVal = EXTI->PR;
	EXTI->PR = regVal;

	// W5500 IRq pin PF2
	if(regVal && EXTI_PR_PR2)
	{
		WIZnet_Chip* WizzNetChip;
		WizzNetChip = WIZnet_Chip::getInstance();
		if(WizzNetChip != NULL)
		{
			WizzNetChip->InterruptHandler();
		}
	}
}

