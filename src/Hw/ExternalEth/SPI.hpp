#ifndef MBED_SPI_H
#define MBED_SPI_H

extern "C"
{
#include <inttypes.h>
#include "stm32f4xx.h"
#include "stm32f429xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_spi.h"

}

class SPI {

public:
    SPI() = default;

    void SetFormat(uint32_t bits, uint32_t mode = 0);

    /** Set the spi bus clock frequency
     *
     *  @param hz SCLK frequency in hz (default = 1MHz)
     */
    uint32_t Write(uint8_t* data, uint16_t len);
    uint8_t WriteRead(uint8_t* rxData, uint8_t* txData, uint16_t len);
    uint32_t Read(uint8_t* data, uint16_t len);

    uint8_t WriteReadDMA(uint8_t *rxData, uint8_t *txData, uint16_t len);
    uint8_t WriteDMA(uint8_t* data, uint16_t len);
    uint8_t ReadDMA(uint8_t* data, uint16_t len);

    void SetPrescaler(uint8_t prescaler);
    uint8_t CalcPrescaler(uint32_t frequency);
    uint32_t GetFrequency();

    uint8_t StoreDatToSend(uint8_t* data,uint16_t len);
    void Init(SPI_HandleTypeDef *hspi);
    ~SPI() = default;
private:

};






#endif
