// modified 08/08/2018 by Bongjun Hur

#ifndef _W5500_
#define _W5500_

#include <cstdlib>
#include <cstring>
#include <inttypes.h>

#include "SPI.hpp"

typedef void (*t_WizzNetIrqCallback)(uint8_t);
typedef void (*t_WizzNetCallback)();

enum Protocol {
	CLOSED = 0,
	TCP    = 1,
	UDP    = 2,
};

enum BufferType {
	RX_BUFFER = 0,
	TX_BUFFER = 1,
};

enum BufferSize {
	BUFF_0K = 0,
	BUFF_1K = 1,
	BUFF_2K = 2,
	BUFF_4K = 4,
	BUFF_8K = 8,
	BUFF_16K = 16
};

typedef enum{
	HEADER_TRANSFER = 0,
	DATA_TRANSFER,
	CLOSE_TRANSFER
}TransferState;

typedef enum{
	HEADER_PREPARED = 0,
	TRANSMIT_COMPLETE,
	RECIEVE_COMPLETE
}TransferEvent;

typedef enum{
	BUFFER_WRITE = 0,
	BUFFER_READ
}TransferDirection;

enum Command {
	OPEN      = 0x01,
	LISTEN    = 0x02,
	CONNECT   = 0x04,
	DISCON    = 0x08,
	CLOSE     = 0x10,
	SEND      = 0x20,
	SEND_MAC  = 0x21,
	SEND_KEEP = 0x22,
	RECV      = 0x40,
};

enum Interrupt {
	INT_CON     = 0x01,
	INT_DISCON  = 0x02,
	INT_RECV    = 0x04,
	INT_TIMEOUT = 0x08,
	INT_SEND_OK = 0x10,
};

enum Status {
	WIZ_SOCK_CLOSED      = 0x00,
	WIZ_SOCK_INIT        = 0x13,
	WIZ_SOCK_LISTEN      = 0x14,
	WIZ_SOCK_SYNSENT     = 0x15,
	WIZ_SOCK_ESTABLISHED = 0x17,
	WIZ_SOCK_CLOSE_WAIT  = 0x1c,
	WIZ_SOCK_UDP         = 0x22,
};


#define DEFAULT_WAIT_RESP_TIMEOUT 500
#define MAX_SOCK_NUM 	8
#define SOCKET_IN_USE 	2

#define MR        0x0000
#define GAR       0x0001
#define SUBR      0x0005
#define SHAR      0x0009
#define SIPR      0x000f
#define SIR       0x0017
#define SIMR      0x0018
#define PHYSTATUS 0x0035

// W5500 socket register
#define Sn_MR         0x0000
#define Sn_CR         0x0001
#define Sn_IR         0x0002
#define Sn_SR         0x0003
#define Sn_PORT       0x0004
#define Sn_DIPR       0x000c
#define Sn_DPORT      0x0010
#define Sn_RXBUF_SIZE 0x001e
#define Sn_TXBUF_SIZE 0x001f
#define Sn_TX_FSR     0x0020
#define Sn_TX_RD      0x0022
#define Sn_TX_WR      0x0024
#define Sn_RX_RSR     0x0026
#define Sn_RX_RD      0x0028
#define Sn_IMR        0x002C

#define BSB_SOCKET_ADDRESS_OFFSET   4
#define BSB_COMMON_REG          0x00
#define BSB_SOC0_REG            0x01
#define BSB_SOC0_TxBUFF         0x02
#define BSB_SOC0_RxBUFF         0x03


class WIZnet_Chip {
public:

    WIZnet_Chip();
    //WIZnet_Chip(SPI* spi, PinName cs, PinName reset);

    virtual ~WIZnet_Chip();

    /*
    * Initialize Wizznet module controller
    *
    * @return true if initialization ok, false if fail
    */
    bool Init();

    /*
    * Set the protocol (UDP or TCP)
    *
    * @param p protocol
    * @ returns true if successful
    */
    bool SetProtocol(uint8_t socket, Protocol p);

    /*
    * Send command for socket driver
    *
    * @return true if write correctly, false otherwise
    */
	bool SocketCommand(uint8_t socket, Command p);

    /*
    * Set source port number for socket
    *
    * @return true if connected, false otherwise
    */ 
	bool SetSourcePort(uint8_t socket, uint16_t port);

    /*
    * Set destination port number for socket
    *
    * @return true if connected, false otherwise
    */
	bool SetDestPort(uint8_t socket, uint16_t port);

    /*
    * Get interrupt register value for selected socket
    *
    * @return true if read OK, false otherwise
    */
	bool GetSocketInteruptReg(uint8_t socket, uint8_t* data);

    /*
    * Reset interrupt register flag for selected socket
    *
    * @return true if reset OK, false otherwise
    */
	bool ResetSocketInteruptStatus(uint8_t socket, uint8_t* value);

    /*
    * Get socket status register for selected socket
    *
    * @return true if read OK, false otherwise
    */
	bool GetSocketStatus(uint8_t socket, uint8_t* value);

    /*
    * Set socket buffer size for selected socket
    *
    * @return true if write OK, false otherwise
    */
	bool SetBufferSize(uint8_t socket, BufferType Type, BufferSize size);

    /*
    * Get socket buffer size for selected socket
    *
    * @return true if read OK, false otherwise
    */
	bool GetBufferSize(uint8_t socket, BufferType Type, BufferSize* size);

    /*
    * Check if socket is in use
    *
    * @return true if used, false otherwise
    */
	bool SocketInUse(uint8_t socket);

    /*
    * Initialize socket
    *
    * @return true if initialize OK, false otherwise
    */
	bool InitSocket(uint8_t socket, Protocol interface, BufferSize RxBufSize, BufferSize TxBufSize, uint16_t sourcePort, uint16_t destinationPort);

    /*
    * Add subscriber for socket irq handler
    *
    * @return true if operation OK, false otherwise
    */
	bool SubscribeSocketIrqHandler(uint8_t socket, t_WizzNetIrqCallback cbFunction);

    /*
    * Get received message size from selected socket
    *
    * @return true if operation OK, false otherwise
    */
	bool GetRecievedMessageSize(uint8_t socket, uint16_t* size);

    /*
    * Get buffer free space in selected socket
    *
    * @return true if operation OK, false otherwise
    */
	bool GetBufferFreeSpace(uint8_t socket, uint16_t* space);

    /*
    * Get read data from selected socket
    *
    * @return true if operation OK, false otherwise
    */
	bool ReadDataFromSocket(uint8_t socket, uint8_t* data, uint16_t dataPointer, uint8_t length);

    /*
    * Get buffer data pointer from selected socket
    *
    * @return true if operation OK, false otherwise
    */
	bool GetBufferDataPointer(uint8_t socket, BufferType type, uint16_t* pData);

    /*
    * Set buffer data pointer in selected socket
    *
    * @return true if operation OK, false otherwise
    */
	bool SetBufferDataPointer(uint8_t socket, BufferType type, uint16_t pData);

    /*
    * Transfer data to tx buffer start from data pointer in selected socket
    *
    * @return true if operation OK, false otherwise
    */
	bool WriteTxBuffer(uint8_t socket, uint16_t dataPointer, uint8_t* txData, uint16_t length, t_WizzNetCallback transferCompletCb);

    /*
    * Transfer data from rx buffer start from data pointer in selected socket
    *
    * @return true if operation OK, false otherwise
    */
	bool ReadRxBuffer(uint8_t socket, uint16_t dataPointer, uint8_t* txData, uint16_t length, t_WizzNetCallback transferCompletCb);

    static WIZnet_Chip * getInstance() {
        return inst;
    };

    enum WIZNET_ERROR : uint8_t
	{
    	NO_CONNECTION = 1,
		WOULD_BLOCK
	};

    uint8_t mac[6];
    uint8_t ip[4];
    uint8_t netmask[4];
    uint8_t gateway[4];
    uint8_t dnsaddr[4];
    bool dhcp;

    uint32_t ReadRegister(uint8_t BSB, uint16_t regAddress, uint8_t* data, uint8_t length);
	uint32_t WriteRegister(uint8_t BSB, uint16_t regAddress, uint8_t* data, uint8_t length);

	uint32_t ReadRegisterTest();

	void InterruptHandler();
	void DataTransferController(TransferEvent event);

protected:
    static WIZnet_Chip* inst;
    SPI* m_SPIIterface;

    void EnableChip();
    void DisableChip();
    void ResetChip();

    bool SetupDataTransfer(uint8_t socket, uint16_t dataPointer, uint8_t* txData, uint16_t length, TransferDirection direction, t_WizzNetCallback transferCompletCb);
    void CreateFrameHeader(uint8_t* const header, uint8_t const BSB, uint16_t const regAddress, TransferDirection const direction);
    uint8_t GetSocketRegisterAddress(uint8_t socket);

    uint8_t m_SocketsInUse;
    uint8_t m_SocketIRqStatus[SOCKET_IN_USE];
    t_WizzNetIrqCallback m_SocketIrqCallback[SOCKET_IN_USE];

    bool m_TransferCtrlBusy = false;
    uint8_t m_TransferDataHeader[3];
    TransferState m_TransferState = HEADER_TRANSFER;
    TransferDirection m_TransferDirection = BUFFER_READ;
    uint8_t* m_pDataTransferBuffer = nullptr;
    uint16_t m_DataTransferLenght = 0U;
    t_WizzNetCallback m_pDataTransferCplCallback = nullptr;
};

#endif
