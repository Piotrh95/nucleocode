/*
 * SPI.cpp
 *
 *  Created on: 19 maj 2021
 *      Author: piotr
 */

#include <cortexm/exception-handlers.h>
#include "SPI.hpp"

extern  SPI_HandleTypeDef hpspi4;
 DMA_HandleTypeDef hdma_spi4_rx;
 DMA_HandleTypeDef hdma_spi4_tx;

 
uint32_t SPI::Write(uint8_t *data, uint16_t len)
{
	HAL_StatusTypeDef ret = HAL_SPI_Transmit(&hpspi4, data, len, 200U);
	return ret;
}


uint32_t SPI::Read(uint8_t *data, uint16_t len)
{
	HAL_StatusTypeDef ret = HAL_SPI_Receive(&hpspi4, data, len, 200U);
	return ret;
}

 
uint8_t SPI::WriteRead(uint8_t *rxData, uint8_t *txData, uint16_t len)
{
	HAL_SPI_TransmitReceive(&hpspi4, txData, rxData, len, 200U);
	return 0;
}


uint8_t SPI::WriteReadDMA(uint8_t *rxData, uint8_t *txData, uint16_t len)
{
	HAL_SPI_TransmitReceive_DMA(&hpspi4, txData, rxData, len);
	return 0;
}


uint8_t SPI::WriteDMA(uint8_t* data, uint16_t len)
{
	HAL_SPI_Transmit_DMA(&hpspi4, data, len);
	return 0;
}


uint8_t SPI::ReadDMA(uint8_t* data, uint16_t len)
{
	HAL_SPI_Receive_DMA(&hpspi4, data, len);
	return 0;
}


void SPI::SetPrescaler(uint8_t prescaler)
{
	uint32_t tempVal = SPI4->CR1 & (~(SPI_CR1_BR_Msk));
	tempVal |= (prescaler << SPI_CR1_BR_Pos);
	SPI4->CR1 = tempVal;
}

uint8_t SPI::CalcPrescaler(uint32_t frequency)
{
	uint32_t AHB_ClockFrequency = 0U;
	AHB_ClockFrequency = HAL_RCC_GetPCLK2Freq();

	uint8_t index;
	for(index = 0U; index < 7U; index++)
	{
		AHB_ClockFrequency = (AHB_ClockFrequency / 2U);
		if(AHB_ClockFrequency < frequency)
			break;
	}
	return index;
}

uint32_t SPI::GetFrequency()
{
	uint32_t AHB_ClockFrequency = 0U;
	AHB_ClockFrequency = HAL_RCC_GetPCLK2Freq();

	uint16_t presc;
	presc =  ((SPI4->CR1 & SPI_CR1_BR_Msk)  >> SPI_CR1_BR_Pos);

	switch(presc)
	{
		case 0: presc = 2U; break;
		case 1: presc = 4U; break;
		case 2: presc = 8U; break;
		case 3: presc = 16U; break;
		case 4: presc = 32U; break;
		case 5: presc = 64U; break;
		case 6: presc = 128U; break;
		case 7: presc = 256U; break;
		default: presc = 2U; break;
	}
	return (AHB_ClockFrequency / presc);
}

void SPI::Init(SPI_HandleTypeDef *hspi)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if (hspi->Instance == SPI4)
	{
		/* Peripheral clock enable */
		__HAL_RCC_SPI4_CLK_ENABLE();
		__HAL_RCC_GPIOE_CLK_ENABLE();

		GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_5 | GPIO_PIN_6;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI4;
		HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

		hspi->Init.Mode = SPI_MODE_MASTER;
		hspi->Init.Direction = SPI_DIRECTION_2LINES;
		hspi->Init.DataSize = SPI_DATASIZE_8BIT;
		hspi->Init.CLKPolarity = SPI_POLARITY_LOW;
		hspi->Init.CLKPhase = SPI_PHASE_1EDGE;
		hspi->Init.NSS = SPI_NSS_SOFT;
		hspi->Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
		hspi->Init.FirstBit = SPI_FIRSTBIT_MSB;
		hspi->Init.TIMode = SPI_TIMODE_DISABLE;
		hspi->Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
		hspi->Init.CRCPolynomial = 10;

		if (HAL_SPI_Init(hspi) != HAL_OK)
		{
			__DEBUG_BKPT();
		}

		__HAL_RCC_DMA2_CLK_ENABLE();

 	    /* SPI4 DMA Init */
 	    /* SPI4_RX Init */
		hdma_spi4_rx.Instance = DMA2_Stream0;
		hdma_spi4_rx.Init.Channel = DMA_CHANNEL_4;
		hdma_spi4_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
		hdma_spi4_rx.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_spi4_rx.Init.MemInc = DMA_MINC_ENABLE;
		hdma_spi4_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
		hdma_spi4_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;

		hdma_spi4_rx.Init.Mode = DMA_NORMAL;
		hdma_spi4_rx.Init.Priority = DMA_PRIORITY_LOW;
		hdma_spi4_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
 	    if (HAL_DMA_Init(&hdma_spi4_rx) != HAL_OK)
 	    {

 	    	__DEBUG_BKPT();
 	    }

 	    __HAL_LINKDMA(hspi,hdmarx,hdma_spi4_rx);

 	    /* SPI4_TX Init */
 	    hdma_spi4_tx.Instance = DMA2_Stream1;
 	    hdma_spi4_tx.Init.Channel = DMA_CHANNEL_4;
 	    hdma_spi4_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
 	    hdma_spi4_tx.Init.PeriphInc = DMA_PINC_DISABLE;
 	    hdma_spi4_tx.Init.MemInc = DMA_MINC_ENABLE;
 	    hdma_spi4_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
 	 	hdma_spi4_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;

	    hdma_spi4_tx.Init.Mode = DMA_NORMAL;
	    hdma_spi4_tx.Init.Priority = DMA_PRIORITY_LOW;
	    hdma_spi4_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
 	    if (HAL_DMA_Init(&hdma_spi4_tx) != HAL_OK)
 	    {

 	    	__DEBUG_BKPT();
 	    }

 	    __HAL_LINKDMA(hspi,hdmatx,hdma_spi4_tx);


 	   //Rx stream
		HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
 
		//Tx stream
		HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 6, 0);
		HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

		SPI4->CR1 |= SPI_CR1_SPE; // SPI4 Module Enable
	}
}

extern "C" {void SPI4_IRQHandler(void);};
extern void SPI4_IRQHandler(void)
{
  HAL_SPI_IRQHandler(&hpspi4);
}
