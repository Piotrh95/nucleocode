/*
 * Utils.c
 *
 *  Created on: 18 cze 2021
 *      Author: piotr
 */

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <typedefs.h>

#include "core_cm4.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_rcc.h"
#include "utils.h"


#define ITM_STIM_U32 (*(volatile unsigned int*)0xE0000000)    // Stimulus Port Register word acces
#define ITM_STIM_U8  (*(volatile         char*)0xE0000000)    // Stimulus Port Register byte acces
#define ITM_ENA      (*(volatile unsigned int*)0xE0000E00)    // Trace Enable Ports Register
#define ITM_TCR      (*(volatile unsigned int*)0xE0000E80)    // Trace control register

char *convert(unsigned int num, int base)
{
    static char Representation[]= "0123456789ABCDEF";
    static char buffer[50];
    char *ptr;

    ptr = &buffer[49];
    *ptr = '\0';

    do
    {
        *--ptr = Representation[num%base];
        num /= base;
    }while(num != 0);

    return(ptr);
}

void DebugCIP( char *fmt, ...)
{
	 char *traverse;
	    unsigned int i;
	    char *s;
	    uint8_t withoutParams = false;
	    //Module 1: Initializing Myprintf's arguments
	    va_list arg;
	    va_start(arg, fmt);
    	if(NULL == strchr(fmt,'%'))
    	{
    	    withoutParams = true;
    	}
	if (!withoutParams) {
		for (traverse = fmt; *traverse != '\0'; traverse++) {

			if (*traverse == '\n') {
				SWO_PrintChar(*traverse);
				return;
			}
			else if(NULL == strchr(traverse,'%'))
			{
				withoutParams = true;
				break;
			}
			while (*traverse != '%') {
				SWO_PrintChar(*traverse);
				traverse++;
			}
			traverse++;

			//Module 2: Fetching and executing arguments
			switch (*traverse) {
			case 'c':
				i = va_arg(arg, int);     //Fetch char argument
				SWO_PrintChar(i);
				break;

			case 'd':
				i = va_arg(arg, int);         //Fetch Decimal/Integer argument
				if (i < 0) {
					i = -i;
					SWO_PrintChar('-');
				}
				SWO_PrintString(convert(i, 10));
				break;

			case 'o':
				i = va_arg(arg, unsigned int); //Fetch Octal representation
				SWO_PrintString(convert(i, 8));
				break;

			case 's':
				s = va_arg(arg, char*);       //Fetch string
				SWO_PrintString(s);
				break;
			case 'l':
				traverse++;
				i = va_arg(arg, unsigned int);
				SWO_PrintString(convert(i, 10));
				break;

			case 'x':
				i = va_arg(arg, unsigned int); //Fetch Hexadecimal representation
				SWO_PrintString(convert(i, 16));
				break;
			}
		}
		if (withoutParams)
		{
			while (*traverse != '\0') {
				SWO_PrintChar(*traverse);
				traverse++;}
			return;

		}
	}
	else
	{
		while (*fmt != '\0') {
			SWO_PrintChar(*fmt);
			fmt++;
		}
	}

	//Module 3: Closing argument list to necessary clean-up
	va_end(arg);
}

void DebugLWIP(char *fmt, ...) {
	char *traverse;
	long i;

	//Module 1: Initializing Myprintf's arguments
	va_list arg;
	va_start(arg, fmt);

	for (traverse = fmt; *traverse != '\0'; traverse++) {
		if (*traverse == '\n') {
			SWO_PrintChar(*traverse);
			return;
		}
		while (*traverse != '%') {
			if (*traverse == '\0') {
				return;
			}
			SWO_PrintChar(*traverse);
			traverse++;
		}

		traverse++;
		if (*traverse == 's') {
			char *s;
			s = va_arg(arg, char*);
			SWO_PrintString(s);

		}
		else if ((*traverse == 'u') || (*traverse == 'x') || (*traverse == 'z')) {
			traverse++;
		}
		i = va_arg(arg, long);
		if (i < 0) {
			i = -i;
			SWO_PrintChar('-');
		}
		if(*traverse == 'p' || *traverse == 'x')
		{
			SWO_PrintString(convert(i, 16));
		}
		else
		{
			SWO_PrintString(convert(i, 10));
		}


	}

	//Module 3: Closing argument list to necessary clean-up
	va_end(arg);
}
void SWO_PrintChar(char c) {
  //
  // Check if ITM_TCR.ITMENA is set
  //
  if ((ITM_TCR & 1) == 0) {
    return;
  }
  //
  // Check if stimulus port is enabled
  //
  if ((ITM_ENA & 1) == 0) {
    return;
  }
  //
  // Wait until STIMx is ready,
  // then send data
  //
  while ((ITM_STIM_U8 & 1) == 0);
  ITM_STIM_U8 = c;
}

void SWO_PrintString(const char *s) {
  //
  // Print out character per character
  //
  while (*s) {
    SWO_PrintChar(*s++);
  }
}
