/*
 * Timer.cpp
 *
 *  Created on: 19 maj 2021
 *      Author: piotr
 */

extern "C"
{
#include "stm32f4xx.h"
#include "stm32f429xx.h"
#include "stm32f4xx_hal.h"
#include <cortexm/exception-handlers.h>

TIM_HandleTypeDef htim1;

}

#include "Timer.h"

Timer::Timer():
startTime(0)
{
	// TODO Auto-generated constructor stub

}

void Timer::Start()
{
	startTime = xTaskGetTickCount();
}

uint32_t Timer::GetTime()
{
	return xTaskGetTickCount() - startTime;
}
void Timer::Reset()
{
	Start();
}

Timer::~Timer()
{
	// TODO Auto-generated destructor stub
}

#if configGENERATE_RUN_TIME_STATS == 1

extern "C"
{
unsigned getRunTimeCounterValue(void)
{
    return htim1.Instance->CNT;
}

void configureTimerForRunTimeStats(void)
{
  __HAL_RCC_TIM2_CLK_ENABLE();
  htim1.Instance = TIM2;
  htim1.Init.Prescaler = 0x0fff;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 4294967295;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV4;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
	  __DEBUG_BKPT();
  }

  HAL_TIM_Base_Start(&htim1);
}

}

#endif
