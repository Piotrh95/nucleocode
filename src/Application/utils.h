/*
 * Utils.h
 *
 *  Created on: 18 cze 2021
 *      Author: piotr
 */

#ifndef APPLICATION_UTILS_H_
#define APPLICATION_UTILS_H_

#include <stdint.h>

void SWO_PrintChar  (char c);
void SWO_PrintString(const char *s);
void DebugCIP(char* fmt, ...);
void DebugLWIP(char* fmt, ...);
#endif /* APPLICATION_UTILS_H_ */
