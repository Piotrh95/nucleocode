/*
 * Timer.h
 *
 *  Created on: 19 maj 2021
 *      Author: piotr
 */

#ifndef APPLICATION_TIMER_H_
#define APPLICATION_TIMER_H_

extern "C"
{
#include "FreeRTOS.h"
#include "timers.h"
#include <inttypes.h>
#include "stm32f4xx_hal_tim.h"

extern TIM_HandleTypeDef htim1;
void ConfigureTimerForRunTimeStats(void);
}

class Timer
{
public:
	Timer();
	void Start();
	uint32_t GetTime();
	void Reset();
	virtual ~Timer();
private :
	TickType_t startTime;
};

#endif /* APPLICATION_TIMER_H_ */
