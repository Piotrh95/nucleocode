
extern "C"
{
#include <stdio.h>
#include <stdlib.h>
#include "diag/trace.h"
#include <FreeRTOS.h>

#include <cortexm/exception-handlers.h>

#include "lwip/init.h"
#include "lwip/netif.h"
#include "lwip/tcpip.h"
#include "ethernetif.h"
#include "networkhandler.h"

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"
#include "opener_api.h"
#include <task.h>
#include "stm32f4xx.h"
#include "stm32f429xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_dma.h"
#include "stm32f4xx_hal_eth.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_spi.h"

#include "utils.h"
extern void vTaskGetRunTimeStats( char *pcWriteBuffer );

}

#include "W5500.h"

extern ETH_HandleTypeDef heth;



extern "C" {
void vApplicationMallocFailedHook( void );
void vApplicationIdleHook( void );
void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName);
}

void vApplicationIdleHook(void)
{

}

void vApplicationMallocFailedHook( void )
{
	__DEBUG_BKPT();
}

void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName )
{
	__DEBUG_BKPT();
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

/* Variables Initialization */
struct netif gnetif;
ip4_addr_t ipaddr;
ip4_addr_t netmask;
ip4_addr_t gw;
uint8_t IP_ADDRESS[4];
uint8_t NETMASK_ADDRESS[4];
uint8_t GATEWAY_ADDRESS[4];

#if configGENERATE_RUN_TIME_STATS == 1
char buf[512];
void vMeasureTimeStatsTask(void *pvParameters)
{
	while (1)
	{
		vTaskDelay(5000UL/portTICK_RATE_MS);
		vTaskGetRunTimeStats(buf);
		std::string Stats(buf);
	}
}
#endif

void InitCIP()
{
	uint8_t pucMACArray[6];
	pucMACArray[0] = 0x00;
	pucMACArray[1] = 0x80;
	pucMACArray[2] = 0xE1;
	pucMACArray[3] = 0x00;
	pucMACArray[4] = 0x00;
	pucMACArray[5] = 0x00;


	char ipAddress[16];
	char netmaskAddress[16];
	char gatewayAddress[16];

	char* address = ip4addr_ntoa(&ipaddr);
	memcpy(ipAddress, address, 16U);

	address = ip4addr_ntoa(&netmask);
	memcpy(netmaskAddress, address, 16U);

	address = ip4addr_ntoa(&gw);
	memcpy(gatewayAddress, address, 16U);


	configureMACAddress(pucMACArray);
	configureNetworkInterface(ipAddress, netmaskAddress, gatewayAddress);
	NetworkHandler_Init();
	CIP_Init(365);
}

void vTaskLwIPStack( void* pvParameters )
{
    configASSERT( ( ( uint32_t ) pvParameters ) == 1 );

    IP_ADDRESS[0] = 192;
    IP_ADDRESS[1] = 168;
    IP_ADDRESS[2] = 2;
    IP_ADDRESS[3] = 10;
    NETMASK_ADDRESS[0] = 255;
    NETMASK_ADDRESS[1] = 255;
    NETMASK_ADDRESS[2] = 255;
    NETMASK_ADDRESS[3] = 0;
    GATEWAY_ADDRESS[0] = 192;
    GATEWAY_ADDRESS[1] = 168;
    GATEWAY_ADDRESS[2] = 2;
    GATEWAY_ADDRESS[3] = 254;

    /* Initilialize the LwIP stack with RTOS */
    tcpip_init( NULL, NULL );

    /* IP addresses initialization without DHCP (IPv4) */
    IP4_ADDR(&ipaddr, IP_ADDRESS[0], IP_ADDRESS[1], IP_ADDRESS[2], IP_ADDRESS[3]);
    IP4_ADDR(&netmask, NETMASK_ADDRESS[0], NETMASK_ADDRESS[1] , NETMASK_ADDRESS[2], NETMASK_ADDRESS[3]);
    IP4_ADDR(&gw, GATEWAY_ADDRESS[0], GATEWAY_ADDRESS[1], GATEWAY_ADDRESS[2], GATEWAY_ADDRESS[3]);

    /* add the network interface (IPv4/IPv6) with RTOS */
    netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);

    /* Registers the default network interface */
    netif_set_default(&gnetif);

    if (netif_is_link_up(&gnetif))
    {
      /* When the netif is fully configured this function must be called */
      netif_set_up(&gnetif);
    }
    else
    {
      /* When the netif link is down this function must be called */
      netif_set_down(&gnetif);
    }

    //start CIP
    InitCIP();

    while(1)
    {
    	NetworkHandler_ProcessOnce();
    }
}

static uint8_t transCounter = 0U;
void TransDMAFinis(void)
{
	transCounter++;
}

void vExtenrnalEthTask( void* pvParameters )
{
	WIZnet_Chip ExternalEthModule;
	ExternalEthModule.Init();
	ExternalEthModule.InitSocket(0U, Protocol::TCP, BufferSize::BUFF_4K, BufferSize::BUFF_4K, 80U, 80U);
	ExternalEthModule.SocketCommand(0U, Command::LISTEN);

	uint8_t debug_trigger = 0U;
	uint8_t regData[6];
	uint8_t readData[100];
	uint16_t messageSize = 0U;
	uint16_t txDataPointer = 0U;

	const char* TestMessage = "Test Message"; // size 12

    while(1)
    {
    	if(debug_trigger == 1U) // Write data to buffer with DMA
    	{
    		debug_trigger = 0U;
    		ExternalEthModule.GetBufferDataPointer(0U, TX_BUFFER, &txDataPointer);
    		ExternalEthModule.WriteTxBuffer(0U, txDataPointer, (uint8_t*)TestMessage, 12U, TransDMAFinis);
    		txDataPointer += 12U; // 12U - message size
    	}

    	if(debug_trigger == 5U) // Update data Tx pointer
    	{
    		debug_trigger = 0U;
    		ExternalEthModule.SetBufferDataPointer(0U, TX_BUFFER, txDataPointer);
    	}

    	if(debug_trigger == 3U) //triger SEND packet
    	{
    		debug_trigger = 0U;
    		ExternalEthModule.SocketCommand(0U, Command::SEND);
    	}

    	if(debug_trigger == 2U) // Read buffer data with DMA
    	{
    		debug_trigger = 0U;

    		uint16_t dataPointer = 0x0000;
    		ExternalEthModule.GetBufferDataPointer(0U, RX_BUFFER, &dataPointer);
    		ExternalEthModule.GetRecievedMessageSize(0U, &messageSize);
    		if(messageSize > 0U && messageSize < 100U)
    		{
    			ExternalEthModule.ReadRxBuffer(0U, dataPointer, readData, messageSize, TransDMAFinis);
    		}
    	}

    	if(debug_trigger == 4U)
    	{
    		debug_trigger = 0U;
    		ExternalEthModule.GetSocketStatus(0U, regData);
    	}
    }
}

extern "C" void InitTaskList(void);
void InitTaskList(void)
{
	BaseType_t xReturned;
	TaskHandle_t xHandle = NULL;

	xReturned = xTaskCreate(vTaskLwIPStack, /* Function that implements the task. */
	"LwIPStackTask", /* Text name for the task. */
	1500, /* Stack size in words, not bytes. */
	(void*) 1, /* Parameter passed into the task. */
	tskIDLE_PRIORITY + 1,/* Priority at which the task is created. */
	&xHandle); /* Used to pass out the created task's handle. */

	xReturned = xTaskCreate(vExtenrnalEthTask, /* Function that implements the task. */
	"vExtenrnalEthTask", /* Text name for the task. */
	256, /* Stack size in words, not bytes. */
	(void*) 1, /* Parameter passed into the task. */
	tskIDLE_PRIORITY + 1,/* Priority at which the task is created. */
	&xHandle); /* Used to pass out the created task's handle. */


#if configGENERATE_RUN_TIME_STATS == 1
    xReturned = xTaskCreate(vMeasureTimeStatsTask,
    "vMeasureTimeStatsTask",
    512,
    (void*) 1,
    tskIDLE_PRIORITY + 1,
    &xHandle);
#endif
}

int main(int argc, char *argv[])
{
	HAL_Init();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	InitTaskList();
	vTaskStartScheduler();

}

extern "C" {void ETH_IRQHandler(void);};
void ETH_IRQHandler(void)
{
	HAL_ETH_IRQHandler(&heth);
}

extern "C" {void Error_Handler(void);};
void Error_Handler()
{

}

#pragma GCC diagnostic pop
